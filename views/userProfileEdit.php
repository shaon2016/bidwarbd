<?php
session_start();
use App\BidWarBd\Auth;
use App\Message\Message;
use App\BidWarBd\User;
use App\BidWarBd\Item;
use App\BidWarBd\BidWarBD;
include_once ('../vendor/autoload.php');
$user=new User();
$user->prepare($_SESSION);
$singleUserInfo=$user->getSingleUserInfo();
//var_dump($singleUserInfo);die();
?>


<!DOCTYPE html>
<html >
<head>
    <meta charset="UTF-8">
    <title>Edit Login Form</title>
    <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="../resources/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="../resources/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

    <link rel="stylesheet" href="../resources/css/normalize.css">
    <link rel="stylesheet" href="../resources/css/style.css">
    <link rel="stylesheet" href="../resources/css/custom-style.css" />
</head>

<body>

<!--<div class="container">


        <h1>EDIT INFO</h1>

        <form enctype="multipart/form-data" action="userProfileUpdate.php" method="post">

                <label>
                    Your Name: <span class="req">*</span>
                </label>

                <label>
                    Email Address<span class="req">*</span>
                </label>

                <label>
                    Mobile Number:<span class="req">*</span>
                </label>

            <label>
                Address:<span class="req">*</span>
            </label>
            <input type="text" required autocomplete="off"  name="address" value="<?php /*echo $singleUserInfo['address']*/?>"/>
            <label>
                District:<span class="req">*</span>
            </label>
                <select class="form-control" name="district">

                </select>

            <?php /*if( !empty($singleUserInfo['image'])){*/?>
            <img src="../resources/images/profile_pictures/">
            <?php /*}*/?>
            <input type="file" name="image">
            <button type="submit" class="button button-block"/>Save Changes</button>
        </form>
</div>-->

<!------------------------------- Start copy --------------------------------->
<div class="form custom-form-boxshadow">
    <div id="signup">
        <h1>Edit Info</h1>

        <form enctype="multipart/form-data" action="userProfileUpdate.php" method="post">
            <input type="hidden" name="id" value="<?php echo $singleUserInfo['id']?>">

            <div class="field-wrap">
                <?php if( !empty($singleUserInfo['image'])){?>
                    <img class="edit-image" src="../resources/images/profile_pictures/<?php echo $singleUserInfo['image']?>" alt="image"/>
                <?php }?>
            </div>

            <div class="field-wrap">
                <label>
                    <!--Your Name: <span class="req">*</span>-->
                </label>
                <input type="text" required autocomplete="off"  name="name" value="<?php echo $singleUserInfo['name']?>"/>
            </div>
            <div class="field-wrap">
                <label>
                    <!--Email Address<span class="req">*</span>-->
                </label>
                <input type="email"required autocomplete="off" name="email" value="<?php echo $singleUserInfo['email']?>"/>
            </div>
           <div class="field-wrap">
                <textarea type="text" name="address"><?php echo $singleUserInfo['address'];?></textarea>
            </div>
            <!--<div class="field-wrap">
                <label>
                    Repeat Password:<span class="req">*</span>
                </label>
                <input type="password" id="txtConfirmPassword" required autocomplete="off" onchange="checkPasswordMatch();"/>
                <div class="registrationFormAlert" id="divCheckPasswordMatch"></div>
            </div>-->
            <div class="field-wrap">
                <label>
                    <!--Mobile Number:<span class="req">*</span>-->
                </label>
                <input type="text" required autocomplete="off"  name="mobile" value="<?php echo $singleUserInfo['mobile']?>"/>
            </div>
            <div class="field-wrap">
                <select class="form-control" name="district">
                    <option <?php if(strtolower($singleUserInfo['district'])=='borishal'){echo 'selected';}?> >Borishal</option>
                    <option <?php if(strtolower($singleUserInfo['district'])=='chittagong'){echo 'selected';}?>>Chittagong</option>
                    <option <?php if(strtolower($singleUserInfo['district'])=='dhaka'){echo 'selected';}?>>Dhaka</option>
                    <option <?php if(strtolower($singleUserInfo['district'])=='khulna'){echo 'selected';}?>>Khulna</option>
                    <option <?php if(strtolower($singleUserInfo['district'])=='rajshahi'){echo 'selected';}?>>Rajshahi</option>
                </select>
            </div>
            <div class="field-wrap">
                <input type="file" name="image">
            </div>
            <button type="submit" class="button button-block"/>Save Changes</button>
        </form>
    </div>
</div>


<!------------------------------- End copy ----------------------------------->

<script src='../resources/js/jquery.js'></script>
<!-- Latest compiled and minified JavaScript -->
<script src="../resources/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<script src="../resources/js/index.js"></script>
<script>
    $('#message').show().delay(3000).fadeOut();
</script>
</body>
</html>


