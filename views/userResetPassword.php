<?php
use App\BidWarBd\Auth;
use App\Message\Message;
use App\BidWarBd\User;
use App\BidWarBd\Item;
use App\BidWarBd\BidWarBD;
include_once ('../vendor/autoload.php');
session_start();
//var_dump($_GET);die();
if(strtoupper($_SERVER['REQUEST_METHOD'])=='POST'){
    $user=new User();
    $_POST['id']=$_SESSION['id'];
    $user->prepare($_POST);
    if($user->resetPassword()){
        $_SESSION['message'] = "success";
        header('location:demo-welcome.php');
    }else{
        $_SESSION['message'] = "failed for network error,try again";
        header('location:demo-welcome.php');
    }
}
?>
<!DOCTYPE html>
<html >
<head>
    <meta charset="UTF-8">
    <title>Sign-Up/Login Form</title>
    <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="../resources/css/normalize.css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="../resources/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="../resources/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="../resources/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="../resources/css/style.css">
    <link rel="stylesheet" href="../resources/css/custom-style.css"/>

</head>

<body>


<!------------------------------------- start ---------------------------------------->

<div class="form custom-form-boxshadow">
    <h1>Change Password</h1>
    <form enctype="multipart/form-data" method="post" action="">

        <input type="hidden" name="user_id" value="<?php echo $_SESSION["id"]; ?>" />
        <div class="field-wrap">
            <label>
                Set A Password<span class="req">*</span>
            </label>
            <input type="password" id="txtNewPassword" required autocomplete="off" name="password"/>
        </div>

        <div class="field-wrap">
            <label>
                Repeat Password:<span class="req">*</span>
            </label>
            <input type="password" id="txtConfirmPassword" required autocomplete="off" onchange="checkPasswordMatch();"/>
            <div class="registrationFormAlert" id="divCheckPasswordMatch"></div>
        </div>
        <button type="submit" class="button button-block">Submit</button>
    </form>
</div>

<!----------------------------------------- end ----------------------------------------->
<script type="text/javascript" src="../resources/js/jquery.js"></script>
<script src="../resources/js/index.js"></script>

<script>
    function checkPasswordMatch() {
        var password = $("#txtNewPassword").val();
        var confirmPassword = $("#txtConfirmPassword").val();

        if (password != confirmPassword)
            $("#divCheckPasswordMatch").html("Passwords do not match!");
        else
            $("#divCheckPasswordMatch").html("Passwords match.");
    }

    $(document).ready(function () {
        $("#txtConfirmPassword").keyup(checkPasswordMatch);
    });

</script>

<script>
    $('#message').show().delay(3000).fadeOut();
</script>

</body>
</html>