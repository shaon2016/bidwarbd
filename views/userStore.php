<?php
use App\BidWarBd\Auth;
use App\Message\Message;
use App\BidWarBd\User;
use App\BidWarBd\Item;
use App\BidWarBd\BidWarBD;
include_once ('../vendor/autoload.php');

session_start();
//var_dump($_POST);die();
$Auth=new Auth();
$User=new User();

$User->prepare($_POST);
$Auth->prepare($_POST);

$checker=$Auth->register();
if($checker==1){
   if($User->register()){
       $_SESSION['email']=$_POST['email'];
       
       $_SESSION['message']="successfully registered";
       header('location:demo-welcome.php');
   }else{
       $_SESSION['message']="registration failed";
       header('location:../index.php');
   }

}elseif($checker==2){
    $_SESSION['message']="Account exists, please login";
    header('location:../index.php');
}else{
    $_SESSION['message']="email is in use";
    header('location:../index.php');
}