<?php
session_start();
use App\BidWarBd\Auth;
use App\Message\Message;
use App\BidWarBd\User;
use App\BidWarBd\Item;
use App\BidWarBd\BidWarBD;
include_once ('../vendor/autoload.php');

$item=new Item();

if(isset($_FILES['product_image']) && !empty($_FILES['product_image']['name']) && $_FILES['product_image']['error']==0){
//image was uploaded
$item->prepare($_GET);
    $oldimage=$item->singleProduct();
    unlink($_SERVER['DOCUMENT_ROOT'].'/BidWarBD/resources/images/uploaded_items/'.$oldimage['product_image']);

    $filename= time().$_FILES['product_image']['product_image'];
$temp_location= $_FILES['product_image']['tmp_name'];
$destination=$_SERVER['DOCUMENT_ROOT']."/BidWarBD/resources/images/uploaded_items/".$filename;
move_uploaded_file($temp_location,$destination); //function takes (temporary location,final location)
$_POST['product_image']=$filename;
$item=new Item();

$item->prepare($_POST);
if($item->itemUpdate()){
$_SESSION['message']= 'item uploaded successfully';
header('location:demo-welcome.php');
//item uploaded to db
}else{
    $_SESSION['message']= 'uploading to database failed';
    header('location:demo-welcome.php');
//uploading item failed
}

}else{
    $item=new Item();

    $item->prepare($_POST);
    if($item->itemUpdate()){
        $_SESSION['message']= 'item uploaded successfully';
        header('location:demo-welcome.php');
//item uploaded to db
    }else{
        $_SESSION['message']= 'uploading to database failed';
        header('location:demo-welcome.php');
//uploading item failed
    }
}