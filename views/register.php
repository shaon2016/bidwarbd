<?php
session_start();
session_destroy();
?>


<!DOCTYPE html>
<html >
<head>
    <meta charset="UTF-8">
    <title>Sign-Up/Login Form</title>
    <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="../resources/css/normalize.css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="../resources/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="../resources/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="../resources/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="../resources/css/style.css">
    <link rel="stylesheet" href="../resources/css/custom-style.css"/>

</head>

<body>

<div class="form custom-form-boxshadow">
    <!--
        <ul class="tab-group">
            <li class="tab active"><a href="#signup">Sign Up</a></li>
           <li class="tab"><a href="#login">Log In</a></li>
    </ul>
-->
<!--    <div class="tab-content"> -->
        <div id="signup">
            <h1>Sign Up for Free</h1>

            <form action="userStore.php" method="post">


                    <div class="field-wrap">
                        <label>
                           Your Name: <span class="req">*</span>
                        </label>
                        <input type="text" required autocomplete="off"  name="name"/>
                    </div>


                <div class="field-wrap">
                    <label>
                        Email Address<span class="req">*</span>
                    </label>
                    <input type="email"required autocomplete="off" name="email"/>
                </div>

                <div class="field-wrap">
                    <label>
                        Set A Password<span class="req">*</span>
                    </label>
                    <input type="password" id="txtNewPassword" required autocomplete="off" name="password"/>
                </div>

                <div class="field-wrap">
                    <label>
                        Repeat Password:<span class="req">*</span>
                    </label>
                    <input type="password" id="txtConfirmPassword" required autocomplete="off" onchange="checkPasswordMatch();"/>
                    <div class="registrationFormAlert" id="divCheckPasswordMatch"></div>
                </div>
                <div class="field-wrap">
                    <label>
                        Mobile Number:<span class="req">*</span>
                    </label>
                    <input type="text" required autocomplete="off"  name="mobile"/>
                </div>
<!--                <div class="field-wrap">-->
<!--                <label>-->
<!--                    District:<span class="req">*</span>-->
<!--                </label>-->
<!--                </div>-->

                <div class="field-wrap">
                    <select class="form-control" name="district">
                        <option>Borishal</option>
                        <option>Chittagong</option>
                        <option>Dhaka</option>
                        <option>Khulna</option>
                        <option>Rajshahi</option>
                    </select>
                </div>

                <button type="submit" class="button button-block"/>Get Started</button>

            </form>

        </div>

     <!--   <div id="login">
            <h1>Welcome Back!</h1>

            <form action="../src/backend/login.php" method="post">

                <div class="field-wrap">
                    <label>
                        Email Address<span class="req">*</span>
                    </label>
                    <input type="email"required autocomplete="off" name="email"/>
                </div>

                <div class="field-wrap">
                    <label>
                        Password<span class="req">*</span>
                    </label>
                    <input type="password"required autocomplete="off" name="password"/>
                </div>

                <p class="forgot"><a href="#">Forgot Password?</a></p>

                <button class="button button-block"/>Log In</button>

            </form>

        </div> -->

    <!-- </div> tab-content -->

</div> <!-- /form http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/ -->

<script src='../resources/js/jquery.js'></script>

<script src="../resources/js/index.js"></script>

<script>
    function checkPasswordMatch() {
        var password = $("#txtNewPassword").val();
        var confirmPassword = $("#txtConfirmPassword").val();

        if (password != confirmPassword)
            $("#divCheckPasswordMatch").html("Passwords do not match!");
        else
            $("#divCheckPasswordMatch").html("Passwords match.");
    }

    $(document).ready(function () {
        $("#txtConfirmPassword").keyup(checkPasswordMatch);
    });

</script>

<script>
    $('#message').show().delay(3000).fadeOut();
</script>


</body>
</html>

