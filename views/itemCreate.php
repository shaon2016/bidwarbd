<?php
session_start();
use App\BidWarBd\Auth;
use App\Message\Message;
use App\BidWarBd\User;
use App\BidWarBd\Item;
use App\BidWarBd\BidWarBD;
include_once ('../vendor/autoload.php');
//add user_id as hidden name="email"
//var_dump($_SESSION);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Item Registration</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="../resources/css/normalize.css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="../resources/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="../resources/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="../resources/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="../resources/css/style.css">
    <link rel="stylesheet" href="../resources/css/custom-style.css"/>

</head>
<body>

<div class="form custom-form-boxshadow">
    <h1>Enter Details of your add:</h1>
    <form enctype="multipart/form-data" method="post" action="itemStore.php">

        <input type="hidden" name="user_id" value="<?php echo $_SESSION["id"]; ?>" />
        <div class="field-wrap">
            <label>
                Item Name:
            </label>
            <input type="text"  name="product_name">
        </div>

        <div class="field-wrap">
            <select name="category_id">
                <option>Item Category:</option>
                <option value="1">books</option>
                <option value="2">Cars & Vehicles</option>
                <option value="3">Electronics</option>
                <option value="4">Furniture</option>
                <option value="5">Properties</option>
                <option value="6">Sports</option>
            </select>
        </div>

        <div class="field-wrap">
            <label>Price for your Item in BDT:</label>
            <input type="number"  name="product_price">
        </div>

        <div class="field-wrap">
            <textarea rows="5" name="product_description" placeholder="Brief description of the item"></textarea>
        </div>
        <label id="expire_day">Enter the last day for auction:</label>
        <div class="field-wrap">
            <input type="date" id="txtdate" name="product_expire_date" >
<!--             for date validation check http://jsfiddle.net/ashishanexpert/LaL9W/5/-->
        </div>

        <input type="file" name="product_image"><span class="registrationFormAlert">MAX: 2MB</span> <br>
        <button type="submit" class="button button-block">Submit</button>
    </form>
</div>

<script src='../resources/js/jquery.js'></script>

<script src="../resources/js/index.js"></script>
</body>
</html>

