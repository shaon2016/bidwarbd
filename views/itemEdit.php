<?php
session_start();
use App\BidWarBd\User;
use App\BidWarBd\Item;
include_once ('../vendor/autoload.php');


$item=new item();
$item->prepare($_GET);
$singleItemInfo=$item->singleProduct();
//var_dump($singleItemInfo);die();
?>


<!DOCTYPE html>
<html >
<head>
    <meta charset="UTF-8">
    <title>Single Item Edit</title>
    <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="../resources/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="../resources/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

    <link rel="stylesheet" href="../resources/css/normalize.css">
    <link rel="stylesheet" href="../resources/css/style.css">
    <link rel="stylesheet" href="../resources/css/custom-style.css" />
</head>

<body>
<!------------------------------- Start copy --------------------------------->
<div class="form custom-form-boxshadow">
    <div id="signup">
        <h1>Edit Item</h1>

        <form enctype="multipart/form-data" action="itemUpdate.php" method="post">
            <input type="hidden" name="id" value="<?php echo $singleItemInfo['id']?>">

            <div class="field-wrap">
                <?php if( !empty($singleItemInfo['product_image'])){?>
                    <img class="edit-image" src="../resources/images/uploaded_items/<?php echo $singleItemInfo['product_image']?>" alt="image"/>
                <?php }?>
            </div>

            <div class="field-wrap">
                <label>
<!--                    Item Name: <span class="req">*</span>-->
                </label>
                <input type="text" required autocomplete="off"  name="product_name" value="<?php echo $singleItemInfo['product_name']?>"/>
            </div>
            <div class="field-wrap">
                <label>
                    <!--Email Address<span class="req">*</span>-->
                </label>
                <textarea name="product_description" > <?php echo $singleItemInfo['product_description']?> </textarea>
            </div>

            <div class="field-wrap">
                <select name="category_id" disabled>
                    <option>Item Category:</option>
                    <option <?php if($singleItemInfo['category_id']==1) echo "selected"?> >books</option>
                    <option <?php if($singleItemInfo['category_id']==2) echo "selected"?> >Cars & Vehicles</option>
                    <option <?php if($singleItemInfo['category_id']==3) echo "selected"?> >Electronics</option>
                    <option <?php if($singleItemInfo['category_id']==4) echo "selected"?> >Furniture</option>
                    <option <?php if($singleItemInfo['category_id']==5) echo "selected"?> >Properties</option>
                    <option <?php if($singleItemInfo['category_id']==6) echo "selected"?> >Sports</option>
                </select>
            </div>

            <div class="field-wrap">
                <label>
                    <!--Mobile Number:<span class="req">*</span>-->
                </label>
                <input type="number" name="price" value="<?php echo $singleItemInfo['product_price']?>" disabled/>
            </div>

            <div class="field-wrap">
                <input type="file" name="product_image">
            </div>
            <button type="submit" class="button button-block"/>Save Changes</button>
        </form>
    </div>
</div>


<!------------------------------- End copy ----------------------------------->

<script src='../resources/js/jquery.js'></script>
<!-- Latest compiled and minified JavaScript -->
<script src="../resources/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<script src="../resources/js/index.js"></script>
<script>
    $('#message').show().delay(3000).fadeOut();
</script>
</body>
</html>


