<?php
session_start();
include_once ('../src/BidWarBd/User.php');
use App\BidWarBd\User;
$user=new User();
$user->prepare($_SESSION);
$singleUserInfo=$user->getSingleUserInfo();
//var_dump($singleUserInfo);die();
?>

<!------------------------------------ modal -------------------------------------->
<!DOCTYPE html>
<html>
<head>
    <meta content="charset=utf-8">
    <title>Bid War Bd</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

    <!--bootstrap-->
    <link rel="stylesheet" href="../resources/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../resources/css/bootstrap-theme.min.css"/>
    <link rel="stylesheet" href="../resources/css/custom-style.css" type="text/css" media="screen" />
</head>
<body>

<!----------------------------------- Start ------------------------------------>
<div class="login-bar">
    <section class="login-section">
        <span class="post-product"><a href="itemCreate.php">Post Your Product</a></span> <!--conditions needed to be added-->
        <div class="logout-span">
            <span class="logout-button"><a href="userLogout.php" >Log Out</a></span><br/>
            <span><a href="userProfileView.php" class="create-new-account"> My Profile</a></span>
        </div>
    </section>
</div>

<div class="container">
    <div class="welcome-custom-container view-full-container-bottom-padding">
        <div class="header-class">
            <a href="demo-welcome.php"><img src="../resources/images/bidWarBd-small-logo.png" class="logo"/></a>
        </div>

        <!-- both left and right container -->
        <div class="left-right-container">
            <!-- right container for dynamic php -->
            <div class="right-container view-item-container">
                <div class="single-per-item-container">
                    <div class="single-img-container">
                        <a class="single-per-item-image">  <img src="../resources/images/profile_pictures/<?php echo $singleUserInfo['image'];?>" class="single-img-tag"/></a>
                    </div>
                    <div class="single-view-per-item-info">

                        <p class="per-item-info-p"><label class="per-item-info-label">User Name: </label><span class="per-item-info-span"> <?php echo $singleUserInfo['name'];?> </span></p>
                        <p class="per-item-info-p"><label class="per-item-info-label">Email: </label><span class="per-item-info-span"> <?php echo $singleUserInfo['email'];?> </span></p>
                        <p class="per-item-info-p"><label class="per-item-info-label">Mobile No: </label><span class="per-item-info-span"> <?php echo $singleUserInfo['mobile'];?> </span></p>
                        <p class="per-item-info-p"><label class="per-item-info-label">District: </label><span class="per-item-info-span"> <?php echo $singleUserInfo['district'];?> </span></p>
                        <p class="per-item-info-p"><label class="per-item-info-label">Address: </label><span class="per-item-info-span"> <?php echo $singleUserInfo['address'];?> </span></p>

                    </div>
                </div>
                <br /><br/>
                <div class="single-item-bottom-buttons-div">
                    <a href="demo-welcome.php" type="button" class="my-button single-item-buttons">Back</a>
                    <a href="userProfileEdit.php" type="button" class="my-button single-item-buttons">Edit Profile</a>
                    <a href="userResetPassword.php" type="button" class="my-button single-item-buttons">Reset Password</a>
                    <a href="singleUserAdd.php?user_id=<?php echo $singleUserInfo['id']?>" type="button" class="my-button single-item-buttons">My Adds</a>
                </div>
            </div>
        </div>
    </div>


    <!-- jQuery -->
    <script type="text/javascript" src="../resources/js/jquery.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
    <script type="text/javascript" src="../resources/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../resources/js/script.js"></script>

    <!----------------------------------- End -------------------------------------->
    
</body>
</html>

