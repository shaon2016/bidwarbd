<?php
//paginator worked on demowelcome only

error_reporting(0);
use App\BidWarBd\Auth;
use App\Message\Message;
use App\BidWarBd\User;
use App\BidWarBd\Item;
use App\BidWarBd\BidWarBD;
include_once ('../vendor/autoload.php');
use App\Utility\Utility;
session_start();

/*this checks if user is logged in using session[email]
if(!isset($_SESSION['email']) && empty($_SESSION['email']) && is_null($_SESSION['email'])){
    header('location:../index.php');
}
*/
$index = new BidWarBD();

if(array_key_exists('category_id',$_GET)) {
    if (!empty($_GET['category_id'])) {
        $_SESSION['category_id'] = $_GET['category_id'];
    }
}else{
    if(!array_key_exists('pageNumber',$_GET)){
        unset($_SESSION['category_id']);}
}


/*
 * shake's
if (!empty($_GET['category_id']) && array_key_exists('category_id', $_GET))
    $_SESSION['category_id'] = $_GET['category_id'];
*/
if (array_key_exists('itemPerPage', $_SESSION)) {
    if (array_key_exists('itemPerPage', $_GET)) {
        $_SESSION['itemPerPage'] = $_GET['itemPerPage'];
    }
} else {
    $_SESSION['itemPerPage'] = 5;
}

//Utility::dd($_SESSION['itemPerPage']);

$itemPerPage = $_SESSION['itemPerPage'];

if (array_key_exists('category_id', $_SESSION))
    $totalItem = $index->countForCategoryIndex($_SESSION['category_id']);
else
    $totalItem = $index->countForLoadIndex();



$totalPage = ceil($totalItem / $itemPerPage);


$pagination = "";


if (array_key_exists('pageNumber', $_GET)) {
    $pageNumber = $_GET['pageNumber'];
} else {
    $pageNumber = 1;
}

if($totalPage>1) {
    for ($i = 1; $i <= $totalPage; $i++) {
        $class = ($pageNumber == $i) ? "active" : "";
        $pagination .= "<li class='$class'><a href='unRegisteredWelcome.php?pageNumber=$i' >$i</a></li>";
    }
}
$pageStartFrom = $itemPerPage * ($pageNumber - 1);


//paginator ends





if (array_key_exists('category_id', $_SESSION)) {
//   $_SESSION['preference']=$_GET['category_id'];

    $index->prepare($_SESSION);
    $categorizedItems = $index->paginateByCategory($pageStartFrom, $itemPerPage);
    if (strtolower($_SERVER['REQUEST_METHOD']) == "post") {
//run functions to sort data by lowest price or highest price
        if ($_POST['filter'] == "lowest_price") {
            $categorizedItems = $index->lowestPriceCategorizedIndex();
        }
        if ($_POST['filter'] == "upcomming_expires") {
            $categorizedItems = $index->expDateCategorizedIndex();
        }
    }
} else {
    $index = new BidWarBD();


// call default index function
    $categorizedItems = $index->paginatorForLoadIndex($pageStartFrom, $itemPerPage);
    if (strtolower($_SERVER['REQUEST_METHOD']) == "post") {
//run functions to sort data by lowest price or highest price
        if ($_POST['filter'] == "lowest_price") {
            $categorizedItems = $index->lowestPriceIndex();
        }
        if ($_POST['filter'] == "upcomming_expires") {
            $categorizedItems = $index->expDateIndex();
        }
    }
}

//var_dump($catagorizedItems);die();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta content="charset=utf-8">
    <title>Bid War Bd</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

    <!--bootstrap-->
    <link rel="stylesheet" href="../resources/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../resources/css/bootstrap-theme.min.css"/>
    <link rel="stylesheet" href="../resources/css/custom-style.css" type="text/css" media="screen" />

</head>

<body>
<div class="login-bar">
    <section class="login-section">

        <span class="post-product"><a href="register.php">Post Your Product</a></span> <!--conditions needed to be added-->

        <form class="login-span" action="userLogin.php" method="post">
            <fieldset>
                <!--<legend>Login information</legend>-->
                Email: <input class="input-fields" type="email" name="email" placeholder="write your email address">
                Password: <input class="input-fields" type="password" name="password">&nbsp;
                <input type="submit" class="login-button" value="Log in">
            </fieldset>
            <a href="register.php"> Create new account</a>
        </form>
    </section>
</div>

<div class="container">
    <div class="welcome-custom-container">
        <div id="message">
            <?php
            if(!empty($_SESSION['message'])) {
                Message::blue($_SESSION['message']);
                $_SESSION['message']="";
            }
            ?>
        </div>

        <header class="header-class">
            <a href="unRegisteredWelcome.php"><img src="../resources/images/bidWarBd-small-logo.png" class="logo"/></a>
        </header>

        <!-- both left and right container -->
        <div class="left-right-container">
            <!-- left container -->
            <div class="left-container">
                <div class="welcome-navigation-container">
                    <form action="" method="post" class="filter-form">
                        <label class="label-color">Filter by</label><br/>
                        <select name="filter" class="filter-class">
                            <option <?php if (!isset($_POST['filter'])) echo "selected" ?> >
                                Oldest First
                            </option>
                            <option
                                value="upcomming_expires" <?php if ($_POST['filter'] == 'upcomming_expires') echo "selected" ?> >
                                Upcomming Expires
                            </option>
                            <option
                                value="lowest_price" <?php if ($_POST['filter'] == 'lowest_price') echo "selected" ?> >
                                Least Price
                            </option>
                        </select>
                        <input type="submit" value="Go" class="logout-button go-button"/>
                    </form>
                    <form action="" method="get" class="filter-form">
                        <label class="label-color">Item Per Page</label><br/>
                        <select name="itemPerPage" class="filter-class">F
                            <option <?php if ($itemPerPage == 5) echo "selected" ?> >5</option>
                            <option <?php if ($itemPerPage == 10) echo "selected" ?> >10</option>
                            <option <?php if ($itemPerPage == 15) echo "selected" ?> >15</option>
                            <option <?php if ($itemPerPage == 20) echo "selected" ?> >20</option>
                            <option <?php if ($itemPerPage == 25) echo "selected" ?> >25</option>
                        </select>
                        <input type="submit" value="Go" class="logout-button go-button"/>
                    </form>

                    <hr/>
                    <div class="all-category"><b>All Categories</b></div>


                    <ul class="welcome-nav-ul">
                        <li class="welcome-category-button"><a href="unRegisteredWelcome.php?category_id=1">Books</a></li>
                        <li class="welcome-category-button"><a href="unRegisteredWelcome.php?category_id=2">Cars and
                                Vehicles</a></li>
                        <li class="welcome-category-button"><a href="unRegisteredWelcome.php?category_id=3">Electronics</a>
                        </li>
                        <li class="welcome-category-button"><a href="unRegisteredWelcome.php?category_id=4">Furniture</a></li>
                        <li class="welcome-category-button"><a href="unRegisteredWelcome.php?category_id=5">Property, Home and
                                Garden</a></li>
                        <li class="welcome-category-button"><a href="unRegisteredWelcome.php?category_id=6">Sports</a></li>
                        <div class="clear"></div>
                    </ul>
                </div>
            </div>


            <!-- right container for dynamic php -->
            <div class="right-container">

                <?php foreach ($categorizedItems as $item) {
                    $sl = 1; ?>
                    <div class="per-item-container">
                        <div>
                            <a class="per-item-image" href="#" data-toggle="modal" data-target="#modal1"> <img
                                    src="../resources/images/uploaded_items/<?php echo $item['product_image']; ?>"
                                    class="single-img-tag"/></a>
                        </div>
                        <div class="per-item-info">
                            <p class="per-item-info-p"><label class="per-item-info-label">Item Name: </label><span
                                    class="per-item-info-span"> <?php echo $item['product_name'] ?> </span></p>

                            <p class="per-item-info-p"><label class="per-item-info-label">Owner's Name: </label><span
                                    class="per-item-info-span"> <?php echo $item['name'] ?> </span></p>

                            <p class="per-item-info-p"><label class="per-item-info-label">Price: </label><span
                                    class="per-item-info-span"> <?php echo $item['product_price'] ?> </span></p>

                            <p class="per-item-info-p"><label class="per-item-info-label">Last Date of
                                    Bid: </label><span
                                    class="per-item-info-span"> <?php echo $item['product_expire_date'] ?> </span></p>

                            <p class="per-item-info-p"><label class="per-item-info-label">District: </label><span
                                    class="per-item-info-span"> <?php echo $item['district'] ?> </span></p>

                            <td><span data-placement="top" data-toggle="tooltip" title="Show Details"><a
                                        class="my-button" href="register.php"> Bid
                                        Now </span></a></span></td>

                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
        <!--<div class="paginator-container">-->
            <div align="right">
                <ul class="pagination">
                    <li <?php if ($pageNumber <= 1) {?> class="disabled" <?php } ?>>
                        <a href="unRegisteredWelcome.php?pageNumber=<?php echo($pageNumber - 1) ?>>">Prev</a></li>
                    <?php echo $pagination ?>
                    <li <?php if ($pageNumber >= $totalPage) { ?> class="disabled" <?php } ?>>
                    <a href="unRegisteredWelcome.php?pageNumber=<?php echo($pageNumber + 1) ?>>">Next</a></li>
                </ul>
            </div>
        <!--</div>-->
    </div>

    <!-- jQuery -->
    <script type="text/javascript" src="../resources/js/jquery.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
    <script type="text/javascript" src="../resources/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../resources/js/script.js"></script>

    <script type="text/javascript">
        $('#message').show().delay(3000).fadeOut(1500);
    </script>

    <!-- jQuery -->
    <script type="text/javascript" src="../resources/js/jquery.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
    <script type="text/javascript" src="../resources/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../resources/js/script.js"></script>
</body>
</html>
