<?php
session_start();
//$_GET["product_id"]; //escape the string if you like
use App\BidWarBd\Auth;
use App\Message\Message;
use App\BidWarBd\User;
use App\BidWarBd\Item;
use App\BidWarBd\BidWarBD;
include_once ('../vendor/autoload.php');

$index=new Item();
$index->prepare($_GET);


$item=$index->singleProduct();
$itemBid=$index->getSingleProductBid();

/*$catagorizedItems=$index->loadIndex($_GET['category_id']);*/
// Run the Query
?>


<!------------------------------------ modal -------------------------------------->
<!DOCTYPE html>
<html>
<head>
    <meta content="charset=utf-8">
    <title>Bid War Bd</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

    <!--bootstrap-->
    <link rel="stylesheet" href="../resources/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../resources/css/bootstrap-theme.min.css"/>
    <link rel="stylesheet" href="../resources/css/custom-style.css" type="text/css" media="screen" />
</head>
<body>

<!----------------------------------- Start ------------------------------------>
<div class="login-bar">
    <section class="login-section">
        <span class="post-product"><a href="itemCreate.php">Post Your Product</a></span> <!--conditions needed to be added-->
        <div class="logout-span">
            <span class="logout-button"><a href="userLogout.php" >Log Out</a></span><br/>
            <span><a href="userProfileView.php" class="create-new-account"> My Profile</a></span>
        </div>
    </section>
</div>

<div class="container">
    <div class="welcome-custom-container view-full-container-bottom-padding">
        <div class="header-class">
            <a href="demo-welcome.php"><img src="../resources/images/bidWarBd-small-logo.png" class="logo"/></a>
        </div>
        <div id="message">
            <?php             if(!empty($_SESSION['message'])) {
                Message::blue($_SESSION['message']);
                $_SESSION['message']="";
            } ?>
        </div>
        <!-- both left and right container -->
        <div class="left-right-container">
            <!-- right container for dynamic php -->
            <div class="right-container view-item-container">
                <div class="single-per-item-container">
                    <div class="single-img-container">
                        <a class="single-per-item-image">  <img src="../resources/images/uploaded_items/<?php echo $item['product_image'];?>" class="single-img-tag"/></a>
                    </div>
                    <div class="single-view-per-item-info">
                        
                        <p class="per-item-info-p"><label class="per-item-info-label">Owner's Name: </label><span class="per-item-info-span"> <?php echo $item['name'];?> </span></p>
                        <p class="per-item-info-p"><label class="per-item-info-label">Product Name: </label><span class="per-item-info-span"> <?php echo $item['product_name'];?> </span></p>
                        <p class="per-item-info-p"><label class="per-item-info-label">Product Description: </label><span class="per-item-info-span"> <?php echo $item['product_description'];?> </span></p>
                        <p class="per-item-info-p"><label class="per-item-info-label">Uploader's Price: </label><span class="per-item-info-span"> <?php echo $item['product_price'];?> </span></p>
                        <p class="per-item-info-p"><label class="per-item-info-label">Expires At: </label><span class="per-item-info-span"> <?php echo $item['product_expire_date'];?> </span></p>
                        <?php if(!is_null($itemBid) or !empty($itemBid)) { ?>
                            <p class="per-item-info-p"><label class="per-item-info-label">Max Bid: </label><span class="per-item-info-span"> <?php echo $itemBid;?> </span></p>
                        <?php } ?>
                    </div>
                    <br/><br/>
                    <div class="single-item-bottom-buttons-div">
                        <a href="demo-welcome.php" type="button" class="my-button single-item-buttons">Back</a>
                        <form method="post" action="submitBid.php" class="single-item-buttons">
                            <input type="hidden" name="product_price" value="<?php echo $item['product_price'];?>" />
                            <input type="hidden" name="product_id" value="<?php echo $_GET['product_id'];?>" />
                            <input type="hidden" name="old_bid" value="<?php echo $itemBid;?>" />
                            <input type="hidden" name="user_id" value="<?php echo $_SESSION['id']?>"/>
                            <input type="hidden" name="product_id" value="<?php echo $item['id']?>"/>
                            <input type="submit" value="Submit Bid" type="button" class="my-button" />
                            <input type="number" name="bid_amount" class="my-button" placeholder="Enter Your Bid Amount">
                        </form>
                    </div>

                </div>

            </div>
        </div>
    </div>


    <!-- jQuery -->
    <script type="text/javascript" src="../resources/js/jquery.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
    <script type="text/javascript" src="../resources/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../resources/js/script.js"></script>
    <script type="text/javascript">
        $('#message').show().delay(3000).fadeOut(1500);
    </script>
    <!----------------------------------- End -------------------------------------->


</body>
</html>

