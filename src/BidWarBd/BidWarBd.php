<?php
namespace App\BidWarBd;
class BidWarBD{
    public $category_id;
    public $filter;
    public $conn;

    public function __construct()
    {
        $this->conn=mysqli_connect('localhost','root','','bidwarbd');
    }
    public function prepare($data=array()){
        if(array_key_exists('category_id',$data)){
            $this->category_id=$data['category_id'];
        }if(array_key_exists('filter',$data)){
            $this->filter=$data['filter'];
        }
    }
    
    public function loadIndex(){
        $indexData=array();
        $query="SELECT * FROM product RIGHT JOIN users ON users.id = product.user_id";
        if($result=mysqli_query($this->conn,$query)){
            while($row=mysqli_fetch_assoc($result)){
                $indexData[]=$row;
            }
            return $indexData;
        }
    }
    
    public function loadCategorizedIndex(){
        $categorizedIndexData=array();
        $query="SELECT product.product_image,product.product_expire_date,users.district,product.product_price,product.id, users.name, product.product_name, category.product_category FROM category JOIN product ON category.id = '".$this->category_id."' AND product.category_id = '".$this->category_id."' INNER JOIN users on users.id = product.user_id";
        if($result=mysqli_query($this->conn,$query)){
            while($row=mysqli_fetch_assoc($result)){
                $categorizedIndexData[]=$row;
            }
            return $categorizedIndexData;
        }
    }

    public function lowestPriceCategorizedIndex(){
        $lowestPriceCategorizedIndex=array();
        $query="SELECT product.product_image,product.product_expire_date,users.district,product.product_price,product.id, users.name, product.product_name, category.product_category FROM category JOIN product ON category.id = '".$this->category_id."' AND product.category_id = '".$this->category_id."' INNER JOIN users on users.id = product.user_id ORDER BY `product`.`product_price` ASC";
        if($result=mysqli_query($this->conn,$query)){
            while($row=mysqli_fetch_assoc($result)){
                $lowestPriceCategorizedIndex[]=$row;
            }
            return $lowestPriceCategorizedIndex;
        }
    }

    public function lowestPriceIndex(){
        $indexData=array();
        $query="SELECT * FROM product INNER JOIN users ON users.id = product.user_id ORDER BY `product`.`product_price` ASC";
        if($result=mysqli_query($this->conn,$query)){
            while($row=mysqli_fetch_assoc($result)){
                $indexData[]=$row;
            }
            return $indexData;
        }
    }

    public function expDateCategorizedIndex(){
        $lowestPriceCategorizedIndex=array();
        $query="SELECT product.product_image,product.product_expire_date,users.district,product.product_price,product.id, users.name, product.product_name, category.product_category FROM category JOIN product ON category.id = '".$this->category_id."' AND product.category_id = '".$this->category_id."' INNER JOIN users on users.id = product.user_id ORDER BY `product`.`product_expire_date` ASC";
        if($result=mysqli_query($this->conn,$query)){
            while($row=mysqli_fetch_assoc($result)){
                $lowestPriceCategorizedIndex[]=$row;
            }
            return $lowestPriceCategorizedIndex;
        }
    }
    
    public function expDateIndex(){
        $indexData=array();
        $query="SELECT * FROM product INNER JOIN users ON users.id = product.user_id WHERE `product_expire_date`>= CURDATE() ORDER By product_expire_date ASC";
        if($result=mysqli_query($this->conn,$query)){
            while($row=mysqli_fetch_assoc($result)){
                $indexData[]=$row;
            }
            return $indexData;
        }
    }

    public function countIndex(){ $query="SELECT COUNT(*) AS totalItem FROM `atomicprojectb22`.`book` WHERE `deleted_at` is null";
        $result=mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row['totalItem'];}
    public function countByCategory(){}
    public function paginateIndex($pageStartFrom=0,$Limit=5){
        $_allBook = array();
        $query="SELECT * FROM `book` WHERE `deleted_at` IS null LIMIT ".$pageStartFrom.",".$Limit;
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_assoc($result)) {
            $_allBook[] = $row;
        }

        return $_allBook;
    }
    
    // Add this below code

    // paginate the index by category
    public function countForCategoryIndex($ID)
    {
        $query = "SELECT COUNT(*) AS totalItem FROM `product`
        where category_id = ". $ID;
        $result = mysqli_query($this->conn, $query);
        $row = mysqli_fetch_assoc($result);
        return $row['totalItem'];
    }

    // paginate the index by category

    public function paginateByCategory($pageStartFrom=0,$Limit=5){

        $_allUser = array();

        $query="SELECT product.product_image,product.product_expire_date,users.district,product.product_price,product.id,
users.name, product.product_name, category.product_category FROM category JOIN product ON category.id = '".$this->category_id."'
AND product.category_id = '".$this->category_id."' INNER JOIN users on users.id = product.user_id LIMIT ".$pageStartFrom.",".$Limit;

        $result= mysqli_query($this->conn,$query);
        while($row = mysqli_fetch_assoc($result)){
            $_allUser[] = $row;
        }
        return $_allUser;
    }


    // default pagination for first load
    public function countForLoadIndex()
    {
        $query = "SELECT COUNT(*) AS totalItem FROM `product`";
        $result = mysqli_query($this->conn, $query);
        $row = mysqli_fetch_assoc($result);
        return $row['totalItem'];
    }

// default pagination for first load

    public function paginatorForLoadIndex($pageStartFrom = 0, $Limit = 5)
    {
        $_allUser = array();
        $query = "SELECT product.product_image,product.product_expire_date,users.district,product.product_price,product.id,
users.name, product.product_name, category.product_category FROM category JOIN product ON category.id = product.category_id INNER JOIN users on users.id = product.user_id LIMIT " . $pageStartFrom . "," . $Limit;
        $result= mysqli_query($this->conn,$query);
        while($row = mysqli_fetch_assoc($result)){
            $_allUser[] = $row;
        }
        return $_allUser;

    }

}