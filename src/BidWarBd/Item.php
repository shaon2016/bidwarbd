<?php
namespace App\BidWarBd;
class Item{
    public $id;
    public $product_id;
    public $product_name;
    public $category_id;
    public $product_description;
    public $product_expire_date;
    public $product_image="";
    public $product_price;
    public $user_id;
    public $product_bid="";
    public $bid_amount;
    public $conn;

    public function __construct()
    {   $this->conn=mysqli_connect("localhost","root","","bidwarbd");
    }
    public function prepare($data=array()){
        if(array_key_exists('product_id',$data)){
            $this->product_id=$data['product_id'];
        };if(array_key_exists('product_name',$data)){
            $this->product_name=$data['product_name'];
        };if(array_key_exists('category_id',$data)){
            $this->category_id=$data['category_id'];};
        if(array_key_exists('product_description',$data)){
            $this->product_description=$data['product_description'];
        };if(array_key_exists('product_expire_date',$data)){
            $this->product_expire_date=$data['product_expire_date'];
        };if(array_key_exists('product_image',$data)){
            $this->product_image=$data['product_image'];
        };if(array_key_exists('product_price',$data)){
            $this->product_price=$data['product_price'];
        };if(array_key_exists('user_id',$data)){
            $this->user_id=$data['user_id'];
        };if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        };if(array_key_exists('product_bid',$data)){
            $this->product_bid=$data['product_bid'];
        }if(array_key_exists('bid_amount',$data)){
            $this->bid_amount=$data['bid_amount'];
        }
    }

    public function register(){
        $price=intval($this->product_price);
        //Enters Item info into database
        $query="INSERT INTO `product` (`category_id`, `user_id`, `product_name`, `product_description`, `product_expire_date`, `product_image`, `product_price`) VALUES ('".$this->category_id."', '".$this->user_id."', '".$this->product_name."', '".$this->product_description."', '".$this->product_expire_date."', '".$this->product_image."', '".$price."')";
        if(mysqli_query($this->conn,$query)){return true;}
        else return false;

    }
    public function getcategoryproduct(){
        //call from startpage to index
        $products=array();
        $query="SELECT * FROM `product` WHERE `category_id`=".$this->category_id;
        if($result=mysqli_query($this->conn,$query)){
            while ($row=mysqli_fetch_assoc($result)){
                $products[]=$row;
            }
            return $products;
        }
    }
    public function listProducts(){
        $products=array();
        $query="SELECT * From `product`  limit 0,6";
        if($result=mysqli_query($this->conn,$query)){
            while ($row=mysqli_fetch_assoc($result)){
                $products[]=$row;
            }
            return $products;
        }
    }
    public function singleProduct(){
        $query="SELECT * from users inner join product on product.user_id = users.id where product.id = ". $this->product_id;
        if($result=mysqli_query($this->conn,$query)){
            $row=mysqli_fetch_assoc($result);
            return $row;
            }
        }

    public function getSingleProductBid() {
        $query = "select bid_amount as max_bid from product inner join product_bid on
      product.id = product_bid.product_id
      where product.id = ".$this->product_id." ORDER BY
       `product_bid`.`bid_amount` DESC";

        if ($result = mysqli_query($this->conn, $query)) {
            $row = mysqli_fetch_assoc($result);
            return $row['max_bid'];
        }
    }

    public function submitBid(){
        $time=time();
        $query="INSERT INTO `bidwarbd`.`product_bid` (`product_id`, `user_id`, `bid_amount`, `bid_time`) VALUES ('".$this->product_id."', '".$this->user_id."', '".$this->bid_amount."', '".$time."')";
        if(mysqli_query($this->conn,$query)){return true;}
        else return false;
    }
    public function itemUpdate(){
        if(!empty($this->product_image)){
            $query="UPDATE `bidwarbd`.`product` SET `product_name` = '".$this->product_name."', `product_description` = '".$this->product_description."', `product_image` = '".$this->product_image."'  WHERE `product`.`id` =".$this->id;            
        }else{
            $query="UPDATE `bidwarbd`.`product` SET `product_name` = '".$this->product_name."', `product_description` = '".$this->product_description."'  WHERE `product`.`id` =".$this->id;
        }
        if(mysqli_query($this->conn,$query)){return true;}
        else return false;
    }

    public function itemDelete(){
        if(!empty($this->bid_amount)){
        $query = "DELETE FROM `product_bid` WHERE `product_id`=".$this->id;
            mysqli_query($this->conn,$query);
        }
        $query="Delete From `product` Where `id`=".$this->id;
        if(mysqli_query($this->conn,$query)){return true;}
        else return false;
    }
}