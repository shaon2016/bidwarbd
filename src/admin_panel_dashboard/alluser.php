<?php
session_start();
include_once ('../../vendor/autoload.php');
use App\BidWarBd\User;
use App\BidWarBd\Auth;
use App\BidWarBd\BidWarBD;
use App\BidWarBd\Item;


$user = new User();

if (array_key_exists('itemPerPage', $_SESSION)) {
    if (array_key_exists('itemPerPage', $_GET)) {
        $_SESSION['itemPerPage'] = $_GET['itemPerPage'];
    }
} else {
    $_SESSION['itemPerPage'] = 5;
}

//Utility::dd($_SESSION['itemPerPage']);

$itemPerPage = $_SESSION['itemPerPage'];
$totalItem = $user->count();
//var_dump($totalItem);
//die();

$totalPage = ceil($totalItem / $itemPerPage);
//Utility::dd($itemPerPage);
$pagination = "";


if (array_key_exists('pageNumber', $_GET)) {
    $pageNumber = $_GET['pageNumber'];
} else {
    $pageNumber = 1;
}
for ($i = 1; $i <= $totalPage; $i++) {
    $class = ($pageNumber == $i) ? "active" : "";
    $pagination .= "<li class='$class'><a href='alluser.php?pageNumber=$i'>$i</a></li>";
}

$pageStartFrom = $itemPerPage * ($pageNumber - 1);
$getAllUserData = $user->paginator($pageStartFrom, $itemPerPage);

?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Simple Responsive Admin</title>
    <!-- BOOTSTRAP STYLES-->
    <link href="assets/css/bootstrap.css" rel="stylesheet"/>
    <link href="style.css" rel="stylesheet" type="text/css">
    <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet"/>
    <!-- CUSTOM STYLES-->
    <link href="assets/css/custom.css" rel="stylesheet"/>
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'/>
</head>
<body>


<div id="wrapper">
    <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="adjust-nav">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">
                    <img src="../../resources/images/bidWarBd-small-logo_old.png"/>
                </a>
            </div>

                 <span class="logout-spn">
                  <a href="#" style="color:#D44B25;">LOGOUT</a>

                </span>
        </div>
    </div>
    <!-- /. NAV TOP  -->
    <nav class="navbar-default navbar-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">


                <li>
                    <a href="index.php"><i class="fa fa-desktop "></i>Dashboard <span class="badge">Included</span></a>
                </li>
                
            </ul>
        </div>

    </nav>
    <!-- /. NAV SIDE  -->


    <div id="page-wrapper">
        <div id="page-inner">

            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <h3>ALL Users Information</h3>
                    <div class="table-responsive">
                        <form role="form">
                            <div class="form-group">
                                <label for="sel1">Select homw many items you want to see (select one):</label>
                                <select class="form-control" id="sel1" name="itemPerPage">
                                    <option

                                        <?php
                                        if ($itemPerPage == 5) {

                                            ?>
                                            selected

                                            <?php
                                        }
                                        ?>


                                    >5
                                    </option>
                                    <option
                                        <?php
                                        if ($itemPerPage == 10) {

                                            ?>
                                            selected

                                            <?php
                                        }
                                        ?>
                                    >10
                                    </option>
                                    <option
                                        <?php
                                        if ($itemPerPage == 15) {

                                            ?>
                                            selected

                                            <?php
                                        }
                                        ?>
                                    >15
                                    </option>
                                    <option
                                        <?php
                                        if ($itemPerPage == 20) {

                                            ?>
                                            selected

                                            <?php
                                        }
                                        ?>
                                    >20
                                    </option>
                                    <option
                                        <?php
                                        if ($itemPerPage == 25) {

                                            ?>
                                            selected

                                            <?php
                                        }
                                        ?>
                                    >25
                                    </option>
                                </select>
                                <button type="submit">Go!</button>

                            </div>
                        </form>
                        <span>Total user, <?php echo $totalItem; ?></span>
                        <table class="table">
                            <thead>
                            <tr class="btn-info">
                                <th>SL</th>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $serialNumber = 1;
                            foreach ($getAllUserData as $user) {
                                ?>
                                <tr class="success">
                                    <td><?php echo $serialNumber++ + $pageStartFrom ?></td>
                                    <td><?php echo $user['id'] ?></td>
                                    <td><?php echo $user['name'] ?></td>
                                    <td><a href="user/details.php?id=<?php echo $user['id'] ?>" class="btn btn-primary"
                                           role="button">Details</a>
                                        <a href="user/ads.php?user_id=<?php echo $user['id'] ?>" role="button"
                                           class="btn btn-success">Ads</a>
                                        <a href="user/banned.php?id=<?php echo $user['id'] ?>" role="button"
                                           class="btn btn-warning">Ban</a>
                                        <a href="deleteUserByAdmin.php?id=<?php echo $user['id'] ?>" role="button"
                                           class="btn btn-danger">Delete</a>
                                    </td>
                                </tr>
                                <?php
                            }
                            ?>
                            </tbody>
                        </table>
                        <div align="right">
                            <ul class="pagination">
                                <li
                                    <?php
                                    if ($pageNumber == 1) {

                                        ?>
                                        class="hidden"

                                        <?php
                                    }
                                    ?>
                                ><a href="alluser.php?pageNumber= <?php echo($pageNumber - 1) ?>>">Prev</a></li>
                                <?php echo $pagination ?>
                                <li
                                    <?php
                                    if ($pageNumber == $totalPage) {

                                        ?>
                                        class="hidden"

                                        <?php
                                    }
                                    ?>
                                ><a href="alluser.php?pageNumber= <?php echo($pageNumber + 1) ?>>">Next</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /. ROW  -->
            <hr>

            <!-- /. ROW  -->

        </div>

        <!-- /. PAGE WRAPPER  -->
        <div class="footer">


            <div class="row">
                <div class="col-lg-12">
                    &copy; 2016 bidwarbd.com | Design by: Extreme Bidders team.
                </div>
            </div>
        </div>


        <!-- /. WRAPPER  -->
        <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
        <!-- JQUERY SCRIPTS -->
        <script src="assets/js/jquery-1.10.2.js"></script>
        <!-- BOOTSTRAP SCRIPTS -->
        <script src="assets/js/bootstrap.min.js"></script>
        <!-- CUSTOM SCRIPTS -->
        <script src="assets/js/custom.js"></script>


</body>
</html>
