﻿<?php
session_start();
include_once ('../../vendor/autoload.php');
use App\BidWarBd\User;
use App\BidWarBd\Auth;
use App\BidWarBd\BidWarBD;
use App\BidWarBd\Item;

if (!isset($_SESSION['admin_email']) && empty($_SESSION['admin_email'])
    && is_null($_SESSION['admin_email'])) {
    header('location:admin_login.php');
}

$user = new User();

if (array_key_exists('itemPerPage', $_SESSION)) {
    if (array_key_exists('itemPerPage', $_GET)) {
        $_SESSION['itemPerPage'] = $_GET['itemPerPage'];
    }
} else {
    $_SESSION['itemPerPage'] = 5;
}

//Utility::dd($_SESSION['itemPerPage']);

$itemPerPage = $_SESSION['itemPerPage'];
$totalItem = $user->count();

$getAllBannedUsers = $user->bannedUsers();
$banned = count($getAllBannedUsers);

$getAllExpiredAds = $user->getUserExpiredAd();
$totalExpiredads = count($getAllExpiredAds);
?>


<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Simple Responsive Admin</title>
    <!-- BOOTSTRAP STYLES-->
    <link href="assets/css/bootstrap.css" rel="stylesheet"/>
    <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet"/>
    <!-- CUSTOM STYLES-->
    <link href="assets/css/custom.css" rel="stylesheet"/>
    <!-- GOOGLE FONTS-->
    <link href="index.php" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'/>
</head>
<body>


<div id="wrapper">
    <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="adjust-nav">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">
                    <img src="../../resources/images/bidWarBd-small-logo_old.png"/>

                </a>

            </div>
              
                <span class="logout-spn">
                  <a href="admin_logout.php" style="color:#;">LOGOUT</a>

                </span>
        </div>
    </div>
    <!-- /. NAV TOP  -->
    <nav class="navbar-default navbar-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">


                <li class="active-link">
                    <a href="index.php"><i class="fa fa-desktop "></i>Dashboard <span class="badge">Included</span></a>
                </li>

            </ul>
        </div>

    </nav>
    <!-- /. NAV SIDE  -->
    <div id="page-wrapper">
        <div id="page-inner">
            <div class="row">
                <div class="col-lg-12">
                    <h2> ADMIN DASHBOARD</h2>
                </div>
            </div>
            <!-- /. ROW  -->
            <hr/>
            <?php if(!empty($_SESSION['message'])){?>
            <div class="row">
                <div class="col-lg-12 ">
                    <div class="alert alert-info" id="message">
                        <strong><?php echo $_SESSION['message']?></strong>
                    </div>

                </div>

            </div>
            <?php $_SESSION['message']="";}?>
            <!-- /. ROW  -->
            <div class="row text-center pad-top">
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                    <div class="div-square">
                        <a href="all_Ads.php">
                            <i class="fa fa-circle-o-notch fa-5x"></i>
                            <h4>All ADs</h4>
                        </a>
                    </div>


                </div>

                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                    <div class="div-square">
                        <a href="Comments.php">
                            <i class="fa fa-comment-o fa-5x"></i>
                            <h4>Comments</h4>
                        </a>
                    </div>


                </div>
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                    <div class="div-square">
                        <a href="userExpiredAds.php">
                            <i class="fa fa-clock-o fa-5x"></i>
                            <h4>Expired Ads  <span class="badge bg-yellow"><?php echo $totalExpiredads; ?></h4>
                        </a>
                    </div>


                </div>
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                    <div class="div-square">
                        <a href="alluser.php">
                            <i class="fa fa-users fa-5x"></i>
                            <h4>All Users  <span class="badge bg-yellow"><?php echo $totalItem; ?></span></h4>
                        </a>
                    </div>


                </div>
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                    <div class="div-square">
                        <a href="bannedUsers.php">
                            <i class="fa fa-ban fa-5x"></i>
                            <h4>Banned Users  <span class="badge bg-yellow"><?php echo $banned; ?></span></h4>
                        </a>
                    </div>


                </div>
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                    <div class="div-square">
                        <a href="#">
                            <i class="fa fa-comments-o fa-5x"></i>
                            <h4>Support</h4>
                        </a>

                    </div>
                </div>


                <!-- /. PAGE INNER  -->
            </div>
            <!-- /. PAGE WRAPPER  -->
        </div>
         <div class="footer">


            <div class="row">
                <div class="col-lg-12">
                    &copy; 2016 bidwarbd.com | Design by: Extreme Bidders team.
                </div>
            </div>
        </div>


        <!-- /. WRAPPER  -->
        <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
        <!-- JQUERY SCRIPTS -->
        <script src="assets/js/jquery-1.10.2.js"></script>
        <!-- BOOTSTRAP SCRIPTS -->
        <script src="assets/js/bootstrap.min.js"></script>
        <!-- CUSTOM SCRIPTS -->
        <script src="assets/js/custom.js"></script>

        <script>
            $('#message').show().delay(3000).fadeOut();

        </script>

</body>
</html>
