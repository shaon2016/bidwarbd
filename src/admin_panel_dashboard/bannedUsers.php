<?php
session_start();
include_once ('../../vendor/autoload.php');
use App\BidWarBd\User;
use App\BidWarBd\Auth;
use App\BidWarBd\BidWarBD;
use App\BidWarBd\Item;

$user = new User();

$getAllBannedUsers = $user->bannedUsers();
$banned = count($getAllBannedUsers);
//var_dump(count($getAllBannedUsers)); die();
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Simple Responsive Admin</title>
    <!-- BOOTSTRAP STYLES-->
    <link href="assets/css/bootstrap.css" rel="stylesheet"/>
    <link href="style.css" rel="stylesheet" type="text/css">
    <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet"/>
    <!-- CUSTOM STYLES-->
    <link href="assets/css/custom.css" rel="stylesheet"/>
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'/>
</head>
<body>


<div id="wrapper">
    <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="adjust-nav">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse"
                        data-target=".sidebar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">
                    <img src="../../resources/images/bidWarBd-small-logo_old.png"/>
                </a>
            </div>

                 <span class="logout-spn">
                  <a href="#" style="color:#D44B25;">LOGOUT</a>

                </span>
        </div>
    </div>
    <!-- /. NAV TOP  -->
    <nav class="navbar-default navbar-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">


                <li>
                    <a href="index.php"><i class="fa fa-desktop "></i>Dashboard <span
                            class="badge">Included</span></a>
                </li>
                

            </ul>
        </div>

    </nav>
    <!-- /. NAV SIDE  -->
    <div id="page-wrapper">
        <div id="page-inner">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <h3>Banned Users List</h3>
                    <div class="table-responsive">


                        <form action="unbanSelected.php" method="post" id='multiple'>
                            <div>
                            <button type="submit" class="btn btn-info" role="button">Unban selected users</button>
                            <button type="button" class="btn btn-info" id="delete_multiple">Delete Selected users</button>
                            </div>

                            <table class="table table-hover">
                                <thead>
                                <tr class="btn-warning">
                                    <th>Check</th>
                                    <th>SL</th>
                                    <th>Name</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>

                                <!--        Here getAllBookData is a object -->
                                <?php
                                $serialNumber = 1;
                                foreach ($getAllBannedUsers as $user) {
                                    ?>
                                    <tr class="success">
                                        <td><input type="checkbox" name="mark[]" value="<?php echo $user->id ?>"></td>
                                        <td><?php echo $serialNumber++ ?></td>
                                        <td><?php echo $user->name ?></td>
                                        <td>
                                            <a href="unban.php?id=<?php echo $user->id ?>" class="btn btn-success"
                                               role="button">Unban</a>
                                            <a href="deleteUsers.php?id=<?php echo $user->id ?>" class="btn btn-danger"
                                               role="button">Delete</a>
                                        </td>
                                    </tr>

                                    <?php
                                }
                                ?>
                                </tbody>
                            </table>


                        </form>

                    </div>
                </div>
            </div>
        </div>


            <!-- /. PAGE WRAPPER  -->
            <div class="footer">


                <div class="row">
                    <div class="col-lg-12">
                        &copy; 2016 bidwarbd.com | Design by: Extreme Bidders team.
                    </div>
                </div>
            </div>


            <!-- /. WRAPPER  -->
            <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
            <!-- JQUERY SCRIPTS -->
            <script src="assets/js/jquery-1.10.2.js"></script>
            <!-- BOOTSTRAP SCRIPTS -->
            <script src="assets/js/bootstrap.min.js"></script>
            <!-- CUSTOM SCRIPTS -->
            <script src="assets/js/custom.js"></script>
            <script>

                // first er ta button id
                // then form id

                $('#delete_multiple').on('click', function () {
                    document.forms[0].action = "deleteSelected.php";
                    $('#multiple').submit();
                });
            </script>


</body>
</html>
