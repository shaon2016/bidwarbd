<?php
session_start();
include_once('../../vendor/autoload.php');
use App\BidWarBd\User;
use App\BidWarBd\Auth;
use App\BidWarBd\BidWarBD;
use App\BidWarBd\Item;

$expired = new User();

$getAllExpiredAds = $expired->getUserExpiredAd();
$totalExpiredads = count($getAllExpiredAds);
//var_dump($getAllExpiredAds); die();

?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Simple Responsive Admin</title>
    <!-- BOOTSTRAP STYLES-->
    <link href="assets/css/bootstrap.css" rel="stylesheet"/>
    <link href="style.css" rel="stylesheet" type="text/css">
    <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet"/>
    <!-- CUSTOM STYLES-->
    <link href="assets/css/custom.css" rel="stylesheet"/>
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'/>
</head>
<body>


<div id="wrapper">
    <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="adjust-nav">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">
                    <img src="../../resources/images/bidWarBd-small-logo_old.png"/>
                </a>
            </div>

                 <span class="logout-spn">
                  <a href="#" style="color:#D44B25;">LOGOUT</a>

                </span>
        </div>
    </div>
    <!-- /. NAV TOP  -->
    <nav class="navbar-default navbar-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">


                <li>
                    <a href="index.php"><i class="fa fa-desktop "></i>Dashboard <span class="badge">Included</span></a>
                </li>

            </ul>
        </div>

    </nav>
    <!-- /. NAV SIDE  -->


    <div id="page-wrapper">
        <div id="page-inner">

            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <h3>ALL Users Information</h3>
                    <a href="deleteExpireProducts.php" role="button"
                       class="btn btn-danger">Delete All Adds</a>
                    <div class="table-responsive">

                        <span>Total Expired ads, <?php echo $totalExpiredads; ?></span>
                        <table class="table">
                            <thead>
                            <tr class="btn-info">
                                <th>SL</th>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Product name</th>
                                <th>Owner's Email</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $serialNumber = 1;
                            foreach ($getAllExpiredAds as $ads) {

                                
                                ?>
                                <tr class="success">
                                    <td><?php echo $serialNumber++ ?></td>
                                    <td><?php echo $ads['id'] ?></td>
                                    <td><?php echo $ads['name'];

                                        ?></td>
                                    <td><?php echo $ads['product_name'] ?></td>
                                    <td><?php echo $ads['email'];

                                        ?></td>
                                    <td>
                                        <a href="mail_owner.php?id=<?php echo $ads['id'] ?>" role="button"
                                           class="btn btn-success">send email to owner</a>
                                        <a href="mail_win_bidder.php?id=<?php echo $ads['id'] ?>" role="button"
                                           class="btn btn-success">send email to win bidder</a>

                                    </td>
                                </tr>
                                <?php
                            }
                            ?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
            <!-- /. ROW  -->
            <hr>

            <!-- /. ROW  -->

        </div>

        <!-- /. PAGE WRAPPER  -->
        <div class="footer">


            <div class="row">
                <div class="col-lg-12">
                    &copy; 2016 bidwarbd.com | Design by: Extreme Bidders team.
                </div>
            </div>
        </div>


        <!-- /. WRAPPER  -->
        <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
        <!-- JQUERY SCRIPTS -->
        <script src="assets/js/jquery-1.10.2.js"></script>
        <!-- BOOTSTRAP SCRIPTS -->
        <script src="assets/js/bootstrap.min.js"></script>
        <!-- CUSTOM SCRIPTS -->
        <script src="assets/js/custom.js"></script>


</body>
</html>
