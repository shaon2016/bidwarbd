<?php

include ('../../../vendor/autoload.php');
use App\BidWarBd\User;


$user = new User();
$user->prepare($_GET);

$allAd = $user->getAdOfAUser();

//var_dump($allAd); die();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta content="charset=utf-8">
    <title>Bid War Bd</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

    <!--bootstrap-->
    <link rel="stylesheet" href="../../../resources/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../../../resources/css/bootstrap-theme.min.css"/>
    <link rel="stylesheet" href="../../../resources/css/custom-style.css" type="text/css" media="screen" />

</head>

<body>
<div class="container">
    <div class="welcome-custom-container">
        <div class="header-class">
            <img src="../../../resources/images/bidWarBd-small-logo.png" class="logo"/>
        </div>

        <!-- both left and right container -->
        <div class="left-right-container">
            <!-- left container -->
            <a href="../../../index.php" role="button"><h3 class="logout-button go-button" >Dashboard</h3></a>
            <div class="left-container">

            </div>
            <div class="clear"></div>

            <!-- right container for dynamic php -->
            <div class="right-container">
                <?php foreach($allAd as $item){?>
                    <div class="per-item-container">
                        <div>
                            <a class="per-item-image" href="#" data-toggle="modal" data-target="#modal1">  <img src="../../../resources/images/uploaded_items/<?php echo $item['product_image'];?>" class="single-img-tag"/></a>
                        </div>
                        <div class="per-item-info">
                            <p class="per-item-info-p"><label class="per-item-info-label">Item Name: </label><span class="per-item-info-span"> <?php echo $item['product_name']?> </span></p>
                            <p class="per-item-info-p"><label class="per-item-info-label">Product Description: </label><span class="per-item-info-span"> <?php echo $item['product_description']?> </span></p>
                            <p class="per-item-info-p"><label class="per-item-info-label">Price: </label><span class="per-item-info-span"> <?php echo $item['product_price']?> </span></p>
                            <p class="per-item-info-p"><label class="per-item-info-label">Last Date of Bid: </label><span class="per-item-info-span"> <?php echo $item['product_expire_date']?> </span></p>
                            <p class="per-item-info-p"><label class="per-item-info-label">Highest Bid: </label><span class="per-item-info-span"> <?php echo $item['district']?> </span></p>


                        </div>
                    </div>
                <?php }?>
            </div>

        </div>
    </div>


    
</div>


<!-- jQuery -->
<script type="text/javascript" src="../../../resources/js/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script type="text/javascript" src="../../../resources/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../../../resources/js/script.js"></script>
</body>
</html>
