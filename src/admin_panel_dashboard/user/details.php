<?php

include_once ('../../../vendor/autoload.php');
use App\BidWarBd\User;
$user=new User();


$user->prepare($_GET);
$singleUserInfo=$user->getSingleUserInfo();
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>My Profile</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../resources/css/bootstrap.min.css">
    <script src="../../../resources/js/jquery.min.js"></script>
    <script src="../../../resources/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>User Profile</h2>
    <div class="container">
        <ul class="nav nav-stacked" x>
            <li class="list-group-item"><img class="img-circle" height="200px" width="200px" src="../../../resources/images/profile_pictures/<?php echo $singleUserInfo['image'];?>" >  </li>
            <li class="list-group-item"> Name: <span><?php echo $singleUserInfo['name'];?></span> </li>
            <li class="list-group-item"> Email:<span><?php echo $singleUserInfo['email'];?></span> </li>
            <li class="list-group-item"> Mobile:<span><?php echo $singleUserInfo['mobile'];?></span> </li>
            <li class="list-group-item"> District:<span><?php echo $singleUserInfo['district'];?></span> </li>
            <li class="list-group-item"> Address:<span><?php echo $singleUserInfo['address'];?></span> </li>
        </ul>

    </div>
</div>


</body>
</html>

