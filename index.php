<?php
session_start();
include_once('vendor/autoload.php');
use App\BidWarBd\User;
use App\BidWarBd\Auth;
use App\BidWarBd\BidWarBD;
use App\BidWarBd\Item;
use App\Message\Message;

$items = new Item();

$allProducts = $items->listProducts();

//var_dump($allProducts); die();
?>

<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
    <meta content="charset=utf-8">
    <title>Bid War Bd</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

    <!--bootstrap-->
    <!--<link rel="stylesheet" href="resources/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="resources/css/bootstrap-theme.min.css"/>-->
    <!-- Syntax Highlighter -->
    <link href="resources/css/shCore.css" rel="stylesheet" type="text/css" />
    <link href="resources/css/shThemeDefault.css" rel="stylesheet" type="text/css" />
    <!-- Demo CSS -->
    <link rel="stylesheet" href="resources/css/custom-style.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resources/css/flexslider.css" type="text/css" media="screen" />

    <!-- Modernizr -->
    <script src="resources/js/modernizr.js"></script>



</head>
<body class="loading">
<div class="login-bar">
    <section class="login-section">
        
        <span class="post-product"><a href="views/register.php">Post Your Product</a></span> <!--conditions needed to be added-->

        <form class="login-span" action="views/userLogin.php" method="post">
            <fieldset>
                <!--<legend>Login information</legend>-->
                Email: <input class="input-fields" type="email" name="email" placeholder="write your email address">
                Password: <input class="input-fields" type="password" name="password">&nbsp;
                <input type="submit" class="login-button" value="Log in">
            </fieldset>
            <a href="views/register.php"> Create new account</a>
        </form>
    </section>
</div>

<div id="container" class="cf custom-container">
    <div id="message">
        <?php if(!empty($_SESSION['message'])) {
            Message::blue($_SESSION['message']);
            $_SESSION['message']="";
        } ?>
    </div>
    <header class="header-class">
        <a href="index.php"><img src="resources/images/bidWarBd-small-logo.png" class="logo"/></a>
    </header>


    <div id="main" role="main">
        <section class="slider custom-flexslider">
            <div class="flexslider slider-max-height-width">
                <ul class="slides">
                    <?php
                    foreach ($allProducts as $item) {
                        ?>
                        <li >
                            <img style="height: 300px; overflow: hidden;"  src="resources/images/uploaded_items/<?php echo $item['product_image'] ?>"/>
                            <div
                                class="flex-caption custom-flex-caption"><?php echo $item['product_price'] . " BDT" ?></div>
                        </li>

                        <?php
                    }
                    ?>
                </ul>
            </div>
        </section>
    </div>

    <section class="navigation-container">
        <div class="nav-section">
            <ul class="nav-ul">
                <li class="category-button"><a href="views/unRegisteredWelcome.php?category_id=1">Books</a></li>
                <li class="category-button"><a href="views/unRegisteredWelcome.php?category_id=2">Cars and Vehicles</a></li>
                <li class="category-button"><a href="views/unRegisteredWelcome.php?category_id=3">Electronics</a></li>
                <li class="category-button"><a href="views/unRegisteredWelcome.php?category_id=4">Furniture</a></li>
                <li class="category-button"><a href="views/unRegisteredWelcome.php?category_id=5">Property, Home and Garden</a></li>
                <li class="category-button"><a href="views/unRegisteredWelcome.php?category_id=6">Sports</a></li>
            </ul>
        </div>
    </section>

</div>


<footer>

</footer>

<!-- jQuery -->
<script type="text/javascript" src="resources/js/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/libs/jquery-1.7.min.js">\x3C/script>')</script>
<script src="resources/js/bootstrap.min.js"></script>


<!-- FlexSlider -->
<script defer src="resources/js/jquery.flexslider.js"></script>

<script type="text/javascript">
    $(function(){
        SyntaxHighlighter.all();
    });
    $(window).load(function(){
        $('.flexslider').flexslider({
            animation: "slide",
            start: function(slider){
                $('body').removeClass('loading');
            }
        });
    });
</script>

<script type="text/javascript">
    $('#message').show().delay(3000).fadeOut(1500);
</script>

<!-- Syntax Highlighter -->
<script type="text/javascript" src="resources/js/shCore.js"></script>
<script type="text/javascript" src="resources/js/shBrushXml.js"></script>
<script type="text/javascript" src="resources/js/shBrushJScript.js"></script>

<!-- Optional FlexSlider Additions -->
<script src="resources/js/jquery.easing.js"></script>
<script src="resources/js/jquery.mousewheel.js"></script>
<script defer src="resources/js/script.js"></script>

</body>
</html>