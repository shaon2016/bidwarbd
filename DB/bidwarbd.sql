-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 15, 2016 at 05:31 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `bidwarbd`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(10) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(40) NOT NULL,
  `password` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `email`, `password`) VALUES
(1, 'bidwarbd', 'bidwarbd@info.com', '12345');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL,
  `product_category` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `product_category`) VALUES
(1, 'books'),
(2, 'vehicle'),
(3, 'electronic'),
(4, 'furniture'),
(5, 'property'),
(6, 'sports');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
`id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_name` varchar(30) NOT NULL,
  `product_description` varchar(200) NOT NULL,
  `product_expire_date` date NOT NULL,
  `product_image` varchar(500) NOT NULL,
  `product_price` int(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `category_id`, `user_id`, `product_name`, `product_description`, `product_expire_date`, `product_image`, `product_price`) VALUES
(11, 3, 10, 'philips monitor', 'best 5k monitor in world', '2016-08-16', 'IMG_20160729_200819.jpg', 19000),
(12, 3, 10, 'micro oven', 'warrenty available, bought 10 days ago, still new, rarely used, selling because need money.', '2016-08-26', 'IMG_20160729_201033.jpg', 8000),
(14, 4, 10, 'sofa', 'one antique sofa', '2016-08-23', '1470286196IMG_20160729_194320.jpg', 12000),
(15, 6, 10, 'football', '', '2016-08-16', '1470300182', 500),
(16, 2, 10, 'car', 'car', '2016-08-24', '1470286294IMG_20160729_194344.jpg', 1600000),
(17, 3, 10, 'headphones', 'beats', '2016-08-23', '1470291649IMG_20160729_200925.jpg', 2500),
(18, 2, 11, 'Cycle', 'Bought new, totally working', '2016-08-23', '1470293030maxit_enduro_50_mx_cycle.jpg', 16000),
(19, 4, 11, 'wardrobe', 'Old wardrobe, recently refreshed', '2016-08-23', '1470293215IMG_20160804_123746.jpg', 10000),
(20, 4, 11, 'single Bed', 'a single bed, tall and strong. no spot or problem. real "shegun" wood', '2016-08-21', '1470293271IMG_20160804_123656.jpg', 20000),
(21, 2, 11, 'CRV', 'used over 2 years, still mint condition, AC not working', '2016-08-20', '1470293314IMG_20160804_123618.jpg', 2000000),
(22, 1, 11, 'Oxford Dictionary', 'Old, but does the job', '2016-08-17', '1470293366IMG_20160804_123527.jpg', 500),
(23, 1, 11, 'edexcel math book', 'C3', '2016-08-21', '1470293402IMG_20160804_123509.jpg', 200),
(24, 1, 11, 'edexcel math book', 'C1', '2016-08-21', '1470293419IMG_20160804_123503.jpg', 300),
(25, 1, 11, 'Algorithm book', 'original Algorithm book', '2016-08-25', '1470293443IMG_20160804_123436.jpg', 500),
(26, 4, 11, 'wardrobe', '3 doors', '2016-08-17', '1470293699IMG_20160729_201011.jpg', 20000),
(27, 3, 10, 'portable harddisk', '160 gb', '2016-08-13', '1470293786IMG_20160729_200906.jpg', 2000),
(28, 3, 10, 'amplifier', 'microlab amp', '2016-08-26', '1470293814IMG_20160729_200857.jpg', 2500),
(29, 3, 10, 'samsung s2', 'used for 3 years, clean set, no problem', '2016-08-31', '1470293872Samsung-Galaxy-S-II-T-Mobile.jpg', 7000),
(41, 5, 15, 'bari', 'bishal ekta bari.', '2016-08-07', '1470667105home3.jpg', 10000000),
(42, 1, 17, 'book abc', ' abc detailed  description ', '2016-08-20', '1471255416', 400);

-- --------------------------------------------------------

--
-- Table structure for table `product_bid`
--

CREATE TABLE IF NOT EXISTS `product_bid` (
`id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `bid_amount` int(11) NOT NULL,
  `bid_time` int(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_bid`
--

INSERT INTO `product_bid` (`id`, `product_id`, `user_id`, `bid_amount`, `bid_time`) VALUES
(5, 11, 10, 20000, 8385959),
(6, 14, 10, 12001, 0),
(7, 18, 10, 17000, 0),
(8, 22, 10, 525, 8385959),
(9, 27, 10, 2003, 8385959),
(10, 11, 11, 20000, 1470300409),
(11, 12, 11, 8005, 1470300427),
(12, 15, 11, 650, 1470300438),
(13, 17, 11, 3000, 1470300444),
(14, 18, 11, 17001, 1470300495),
(15, 21, 11, 2000001, 1470300510),
(20, 41, 15, 10000001, 1470667246),
(21, 41, 10, 10000002, 1470667307),
(22, 22, 17, 530, 1471247483),
(23, 22, 17, 540, 1471247661);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` varchar(50) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `district` varchar(50) NOT NULL,
  `address` varchar(50) NOT NULL,
  `image` varchar(500) NOT NULL,
  `ban` varchar(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `mobile`, `district`, `address`, `image`, `ban`) VALUES
(1, 'shaon', 'ashiq201@yahoo.com', 'ashiq201', '01829697802', 'Chittagong', 'pahartali', '', NULL),
(2, 'ashiq', 'ashiq@gmail.com', 'ashiq201', '12345', 'Dhaka', 'Savar', '', NULL),
(10, 'ramim', 'r@gmail.com', '674f3c2c1a8a6f90461e8a66fb5550ba', '564231', 'Chittagong', 'masjid galli', '1469957403IMG_20160729_200857.jpg', NULL),
(11, 'alpha user', 'alpha@gmail.com', '2c1743a391305fbf367df8e4f069f9f9', '01680445592', 'Chittagong', '', '', NULL),
(14, 'eee', 'eee@eee.eee', 'd2f2297d6e829cd3493aa7de4416a18f', '123456', 'Chittagong', '', '', NULL),
(15, 'Sumayel', 'sumayel@yahoo.com', '202cb962ac59075b964b07152d234b70', '1234567890', 'Chittagong', 'xgsf dfndhjet  dghgtytj', '1470666957person3.jpg', NULL),
(16, 'aaa', 'aaa@aaa.aaa', '47bce5c74f589f4867dbd57e9ca9f808', '1234567890', 'Chittagong', '', '', '1471267279'),
(17, 'abc', 'abc@abc.abc', '900150983cd24fb0d6963f7d28e17f72', '1234567890', 'Chittagong', 'xyz xyz xyz xyz', '1471205774person4.png', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
 ADD PRIMARY KEY (`id`), ADD KEY `category_id` (`category_id`), ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `product_bid`
--
ALTER TABLE `product_bid`
 ADD PRIMARY KEY (`id`), ADD KEY `product_id` (`product_id`), ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `product_bid`
--
ALTER TABLE `product_bid`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
